import { GraphQLServer } from 'graphql-yoga'

const users = [
    {
        id: "1",
        name : "Kurniadi",
        email: "Kurniadiahmadwijaya@gmail.com"
    },
    {
        id: "2",
        name: "Aang",
        email: "bukanavatar@gmail.com"
    },
    {
        id: "3",
        name: "Adrian",
        email: "rianhayden@gmail.com"
    }
]

const buku = [
    {
        id: 1,
        judul: "Halo Dunia",
        harga: 90.000,
        tahunRilis: 2017,
        stok: true,
        author: "1"
    },
    {
        id: 3,
        judul: "Python Dasar",
        harga: 70.000,
        tahunRilis: 2019,
        rating: 4.9,
        stok: false,
        author: "2"
    }
]
// Definition
const typeDefs = `
    type Query{
        users(query: String): [User!]!
        buku(query: String): [Buku!]!
        kategori: Kategori!
    }

    type Buku{
        id: ID!
        judul: String!
        harga: Float!
        tahunRilis: Int!
        rating: Float
        stok: Boolean!
        author : User
    }
    type User {
        id: ID!
        name: String!
        email: String!
        age: Int
        buku: [Buku!]!
    }
    
    type Kategori{
        id: ID!
        judul: String!
    }

`

// Resolver
const resolvers = {
    Query: {
        users(parent, args, ctx, info){
            if(!args.query){
                return users
            }else{
                return users.filter((user)=> {
                    return user.name.toLowerCase().includes(args.query.toLowerCase())
                })
            }
        },
        buku(parent, args, ctx, info) {
            if (!args.query) {
                return buku
            } else {
                return buku.filter((buku) => {
                    return buku.judul.toLowerCase().includes(args.query.toLowerCase())
                })
            }
        },
        kategori() {
            return{
                id: 2,
                judul: "Ilmu Pengetahuan"
            }
        }

    },
    Buku:{
        author(parent, args, ctx, info) {
            return users.find((user) => {
                return user.id === parent.author
            })
        }
    },
    User:{
        buku(parent, args, ctx, info){
            return buku.filter((buku) => {
                return buku.author === parent.id
            })
        }
    }
}

const server = new GraphQLServer({
    typeDefs,
    resolvers
})

server.start(() => {
    console.log('Server berjalan')
})